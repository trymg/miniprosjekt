//@flow
//eslint eqeqeq: "off"
import {Sak, sakService} from './services';
import {Alert, Card, NavBar, Button, Row, Column} from './widgets';


import * as React from 'react';
import {Component} from 'react-simplified';
import {HashRouter, Route, NavLink} from 'react-router-dom';
import ReactDOM from 'react-dom';

// Reload application when not in production environment
if (process.env.NODE_ENV != 'production') {
    let script = document.createElement('script');
    script.src = '/reload/reload.js';
    if (document.body) document.body.appendChild(script);
}

import {createHashHistory} from 'history';

const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

// navigation bar
class Meny extends Component {
    render() {
        return (
            <NavBar brand="Dataing-posten">
                <NavBar.Link to='/sak/ny'>+Ny sak</NavBar.Link>

                <NavBar.Link to='/sak/kategori/Sport'>Sport</NavBar.Link>
                <NavBar.Link to='/sak/kategori/Teknologi'>Teknologi</NavBar.Link>
                <NavBar.Link to='/sak/kategori/Musikk'>Musikk</NavBar.Link>
            </NavBar>
        );
    }
}

// live feed with the newest articles
class Livefeed extends Component {
    saker: Sak[] = [];

    render() {
        return (
            <div>
                <center><h3>Live-feed</h3></center>
                <div style={{
                    height: '162px',
                    width: '90%',
                    border: '1px solid #ccc',
                    overflowY: 'hidden',
                    whiteSpace: 'nowrap',
                    margin: 'auto',
                    marginBottom: '12px'
                }}>
                    {this.saker.map(sak => (
                        <div key={sak.sak_id} className={"card"}
                             style={{height: '150px', width: '20rem', display: 'inline-block'}}>
                            <div className="card-body">
                                <h5 className="card-title">{sak.overskrift.substr(0, 17)}</h5>
                                <p className="card-text">{sak.tidsstempel.substr(0, 16)}</p>
                                <NavLink className="btn btn-primary" to={'/sak/' + sak.sak_id}>Gå til sak</NavLink>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }


    componentDidMount() {
        sakService
            .getNyesteSaker()
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
        this.interval = setInterval(() => {
            sakService
                .getNyesteSaker()
                .then(saker => (this.saker = saker))
                .catch((error: Error) => Alert.danger(error.message));
        }, 10000)
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
}

// main feed for the articles with importance '1'
class Hovedfeed extends Component {
    saker: Sak[] = [];

    render() {
        return (
            <div>
                <center><h3>Hoved-feed</h3></center>
                <div className="container">
                    <div className="row">
                        <div className="col-">
                        </div>
                        <div className="col-lg">
                            <ul style={{listStyle: "none"}}>
                                {this.saker.map((sak) => (
                                    <li key={sak.sak_id}>
                                        <br></br>
                                        <div className={"card"}>
                                            <img src={sak.bildelink} className={"card-img-top"} alt={sak.alt_tekst}/>
                                            <div className="card-body">
                                                <h5 className="card-title">{sak.overskrift.substr(0, 50)}</h5>
                                                <p className="card-text">{sak.tidsstempel.toString().substr(0, 16)}</p>
                                                <NavLink className="btn btn-primary" to={'/sak/' + sak.sak_id}>Gå til
                                                    sak</NavLink>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="col-">
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    mounted() {
        sakService
            .getViktigeSaker()
            .then(saker => (this.saker = saker))
            .catch((error: Error) => Alert.danger(error.message));
    }
}

// component for displaying each individual article
class EnkelSak extends Component<{ match: { params: { id: number } } }> {
    saker: Sak[] = [];

    render() {

        let sak = this.saker.find(sak => sak.sak_id == this.props.match.params.id);
        if (!sak) {
            console.error('sak ikke funnet');
            return null;
        }
        return (
            //sak
            //columns (only middle is used, outer ones are for margin
            <div className="container">
                <div className="row">
                    <div className="col-">
                    </div>
                    <div className="col-lg">
                        <br/>
                        <div className="card">
                            <img src={sak.bildelink} className="card-img-top" alt={sak.alt_tekst}/>
                            <div className="card-body">
                                <h5 className="card-title">{sak.overskrift}</h5>
                                <p className="card-text">{sak.innhold}</p>
                                <p className="card-text"><small
                                    className="text-muted">{sak.tidsstempel.toString().substr(0, 16)}</small></p>
                                <p className="card-text"><small className="text-muted">Artikkelnr.: {sak.sak_id}</small>
                                </p>
                                <NavLink className="btn btn-secondary"
                                         to={'/sak/rediger/' + sak.sak_id}>Rediger</NavLink>
                                <Button.Danger className="btn btn-danger" onClick={this.save}>Slett</Button.Danger>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-"></div>
            </div>
        );
    }

    mounted() {
        sakService
            .getViktigeSaker(this.props.match.params.id)
            .then(saker => (this.saker = saker))
            .catch((error: Error) => console.log(error.message));
    }

    save() {
        sakService
            .slettSak(this.props.match.params.id)
            .then(history.push('/'))
            .catch((error: Error) => console.log(error.message));
    }
}

// form for creating and uploading a new article
class NySak extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-">
                    </div>
                    <div className="col-lg">
                        <br/>
                        <form>
                            <h1>Legg til ny sak</h1>
                            <br></br>
                            <div className="form-group">
                                <label htmlFor="overskriftInput">Overskrift:</label>
                                <input type="text" className="form-control" id="overskriftInput"
                                       aria-describedby="overskriftHelp" placeholder="Skriv inn tittel"></input>
                                <small id="overskriftHelp" className="form-text text-muted">Tittelen på
                                    artikkelen.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="innholdInput">Innhold:</label>
                                <input type="text" className="form-control" id="innholdInput"
                                       aria-describedby="innholdHjelp" placeholder="Skriv inn innhold"></input>
                                <small id="innholdHjelp" className="form-text text-muted">Innholdet i artikkelen (maks
                                    255 tegn).</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bildelinkInput">Bildelink:</label>
                                <input type="text" className="form-control" id="bildelinkInput"
                                       aria-describedby="bildeLinkHjelp"
                                       placeholder="Lim inn url til artikkelbildet"></input>
                                <small id="bildeLinkHjelp" className="form-text text-muted">url til bildet i
                                    artikkelen.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="kategoriInput">Kategori:</label>
                                <input type="text" className="form-control" id="kategoriInput"
                                       aria-describedby="kategoriHjelp" placeholder="Skriv inn en kategori"></input>
                                <small id="kategoriHjelp" className="form-text text-muted">kategorien artikkelen skal
                                    stå under (en av følgende: Sport, Teknologi, Musikk).</small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="alt_tekstInput">Alt-tekst:</label>
                                <input type="text" className="form-control" id="alt_tekstInput"
                                       aria-describedby="alt_tekstHjelp"
                                       placeholder="Skriv inn en alt-tekst til bildet"></input>
                                <small id="alt_tekstHjelp" className="form-text text-muted">Alternativ tekst til bildet
                                    for blinde/svaksynte</small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="viktighetInput">Viktighet:</label>
                                <input type="text" className="form-control" id="viktighetInput"
                                       aria-describedby="viktighetHjelp" placeholder="Skriv inn viktighet"></input>
                                <small id="viktighetHjelp" className="form-text text-muted">Viktighet '1' eller '2'
                                    (artikler med viktighet 1 vises på hovedsiden).</small>
                            </div>
                            <button className="btn btn-primary" onClick={this.save}>Send</button>
                            <NavLink className="btn btn-secondary" to={'/'}>Avbryt</NavLink>
                        </form>
                    </div>
                </div>
                <div className="col-"></div>
            </div>
        );
    }

    save() {
        if ((document.getElementById("overskriftInput").value) !== '' && (document.getElementById("innholdInput").value) !== '' && (document.getElementById("bildelinkInput").value) !== '' && (document.getElementById("kategoriInput").value) !== '' && (document.getElementById("alt_tekstInput").value) !== '' && (document.getElementById("viktighetInput").value) !== '') {

            let sak = new Sak(document.getElementById("overskriftInput").value, document.getElementById("innholdInput").value, document.getElementById("bildelinkInput").value, document.getElementById("kategoriInput").value, document.getElementById("alt_tekstInput").value, document.getElementById("viktighetInput").value)
            sakService
                .lastOppSak(sak)
                .then(() => history.push('/'))
                .catch((error: Error) => console.log(error.message));
        } else {
            (Alert.danger('fyll inn alle felter'));
        }
    }
}


// form to edit an article
class RedigerSak extends Component<{ match: { params: { id: number } } }> {
    sak = null; //undefined

    render() {
        if (!this.sak) return null;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-">
                    </div>
                    <div className="col-lg">
                        <br/>
                        <form>
                            <h1>Rediger sak {this.sak.sak_id}</h1>
                            <br></br>
                            <div className="form-group">
                                <label htmlFor="overskriftInput">Overskrift:</label>
                                <input type="text" className="form-control" id="overskriftInput"
                                       aria-describedby="overskriftHelp" value={this.sak.overskrift}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           if (this.sak) this.sak.overskrift = event.target.value;
                                       }}></input>
                                <small id="overskriftHelp" className="form-text text-muted">Tittelen på
                                    artikkelen.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="innholdInput">Innhold:</label>
                                <input type="text" className="form-control" id="innholdInput"
                                       aria-describedby="innholdHjelp" value={this.sak.innhold}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           if (this.sak) this.sak.innhold = event.target.value;
                                       }}></input>
                                <small id="innholdHjelp" className="form-text text-muted">Innholdet i artikkelen (maks
                                    255 tegn).</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bildelinkInput">Bildelink:</label>
                                <input type="text" className="form-control" id="bildelinkInput"
                                       aria-describedby="bildeLinkHjelp" value={this.sak.bildelink}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           if (this.sak) this.sak.bildelink = event.target.value;
                                       }}></input>
                                <small id="bildeLinkHjelp" className="form-text text-muted">url til bildet i
                                    artikkelen.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="kategoriInput">Kategori:</label>
                                <input type="text" className="form-control" id="kategoriInput"
                                       aria-describedby="kategoriHjelp" value={this.sak.kategori}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           if (this.sak) this.sak.kategori = event.target.value;
                                       }}></input>
                                <small id="kategoriHjelp" className="form-text text-muted">kategorien artikkelen skal
                                    stå under (en av følgende: Sport, Teknologi, Musikk).</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="viktighetInput">Viktighet:</label>
                                <input type="text" className="form-control" id="viktighetInput"
                                       aria-describedby="viktighetHjelp" value={this.sak.viktighet}
                                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                           if (this.sak) this.sak.viktighet = Number(event.target.value);
                                       }}></input>
                                <small id="viktighetHjelp" className="form-text text-muted">Viktighet '1' eller '2'
                                    (artikler med viktighet 1 vises på hovedsiden).</small>
                            </div>
                            <button className="btn btn-primary" onClick={this.save}>Oppdater</button>
                            <NavLink className="btn btn-secondary" to={'/sak/' + this.sak.sak_id}>Avbryt</NavLink>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    mounted() {
        sakService
            .getSak(this.props.match.params.id)
            .then(sak => (this.sak = sak))
            .catch((error: Error) => console.log(error.message));
    }

    save() {
        if (!this.sak) return null;

        sakService
            .oppdaterSak(this.sak)
            .then(() => {
                let sakListe = Hovedfeed.instance();
                if (sakListe) sakListe.mounted(); //update sak list
                if (this.sak) history.push('/sak/' + this.sak.sak_id);
            })
            .catch((error: Error) => console.log(error.message));
    }
}


class KategoriFeed extends Component<{ match: { params: { kategori: string } } }> {
    saker: Sak[] = [];

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-">
                        </div>
                        <div className="col-lg">
                            <ul style={{listStyle: "none"}}>
                                {this.saker.map((sak) => (
                                    <li key={sak.sak_id}>
                                        <br></br>
                                        <div className={"card"}>
                                            <img src={sak.bildelink} className={"card-img-top"} alt={sak.alt_tekst}/>
                                            <div className="card-body">
                                                <h5 className="card-title">{sak.overskrift}</h5>
                                                <p className="card-text">{sak.tidsstempel.toString().substr(0, 16)}</p>
                                                <NavLink className="btn btn-primary" to={'/sak/' + sak.sak_id}>Gå til
                                                    sak</NavLink>
                                            </div>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="col-">
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    mounted() {
        sakService
            .getCategory(this.props.match.params.kategori)
            .then(saker => (this.saker = saker))
            .catch((error: Error) => console.log(error.message));
    }
}


const root = document.getElementById('root');
if (root)
    ReactDOM.render(
        <HashRouter>
            <div>
                <Alert/>
                <Route path={"/"} component={Meny}/>
                <Route exact path={'/'} component={Livefeed}/>
                <Route exact path={"/"} component={Hovedfeed}/>
                <Route exact path={"/sak/:id"} component={EnkelSak}/>
                <Route exact path={"/sak/ny"} component={NySak}/>
                <Route exact path={'/sak/rediger/:id'} component={RedigerSak}/>
                <Route exact path={'/sak/kategori/:kategori'} component={KategoriFeed}/>
            </div>
        </HashRouter>,
        root
    );