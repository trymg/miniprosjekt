// @flow
import axios from 'axios';

export class Sak {
    sak_id: number;
    static nextId = 1;
    // sak_id = 1;

    overskrift: string;
    innhold: string;
    tidsstempel: string;
    bildelink: string;
    kategori: string; //Teknologi, Sport, Musikk
    alt_tekst: string;
    viktighet: number;

    constructor(overskrift: string, innhold: string, bildelink: string, kategori: string, alt_tekst: string, viktighet: number) {
        this.overskrift = overskrift;
        this.innhold = innhold;
        this.bildelink = bildelink;
        this.kategori = kategori;
        this.alt_tekst = alt_tekst;
        this.viktighet = viktighet;
    }
}

class SakService {
    getViktigeSaker() {
        return axios.get<Sak[]>('http://localhost:3001/sak/viktighet/1').then(response => response.data);
    }

    slettSak(id: number) {
        return axios.delete<Sak>('http://localhost:3001/sak/slett/' + id).then(response => response.data);
    }

    getNyesteSaker() {
        return axios.get<Sak[]>('http://localhost:3001/sak/dato/nyeste').then(response => response.data);
    }

    getSak(id: number) {
        return axios.get<Sak>('http://localhost:3001/sak/' + id).then(response => response.data);
    }

    oppdaterSak(sak: Sak) {
        return axios.put<Sak, void>('/sak/oppdater/', sak).then(response => response.data);
    }

    getCategory(kategori: string) {
        return axios.get<Sak[]>('http://localhost:3001/sak/kategori/' + kategori).then(response => response.data);
    }

    lastOppSak(sak: Sak) {
        return axios.post<Sak, void>('http://localhost:3001/sak/ny', sak).then(response => response.data);
    }
}

export let sakService = new SakService();