//@flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';

var mysql = require("mysql");
const public_path = path.join(__dirname, '/../../client/public');
var app = express();
var cors = require("cors");
var bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(public_path));
app.use(express.json());
import {type Sak, SakerModel, syncModels} from './models.js';


// gets individual article
app.get('/sak/:id', (req: express$Request, res: express$Response) => {
    return SakerModel.findOne({where: {sak_id: Number(req.params.id)}})
        .then(sak => sak ? res.send(sak) : res.sendStatus(404))
});


// posts new article
app.post('/sak/ny', function (request, response) {
    return SakerModel.create({
        overskrift: request.body.overskrift,
        innhold: request.body.innhold,
        bildelink: request.body.bildelink,
        kategori: request.body.kategori,
        alt_tekst: request.body.alt_tekst,
        viktighet: request.body.viktighet
    }).then(function (SakerModel) {
        if (SakerModel) {
            response.send(SakerModel);
        } else {
            response.status(400).send('Error in insert new sak');
        }
    });
});


// deletes an article
app.delete('/sak/slett/:id', (req: express$Request, res: express$Response) => {
    return SakerModel.destroy({where: {sak_id: Number(req.params.id)}})
});


// gets important articles
app.get('/sak/viktighet/1', (req: express$Request, res: express$Response) => {
    return SakerModel.findAll({
        where: {viktighet: Number(1)},
        limit: 35
    })
        .then(sak => sak ? res.send(sak) : res.sendStatus(404)
        );
});


// updates an article
app.put('/sak/oppdater', (req: { body: Sak }, res: express$Response) => {
    console.log('fikk update-request fra klient')
    // Respond with error if id is missing
    if (!req.body.sak_id) return res.sendStatus(400);

    return SakerModel.update(
        {
            overskrift: req.body.overskrift,
            innhold: req.body.innhold,
            bildelink: req.body.bildelink,
            kategori: req.body.kategori,
            alt_tekst: req.body.alt_tekst,
            viktighet: req.body.viktighet
        },
        {where: {sak_id: req.body.sak_id}}
    ).then(updated => (updated[0] /* affected rows */ > 0 ? res.sendStatus(200) : res.sendStatus(404)));
});


// gets the newest articles
app.get('/sak/dato/nyeste', (req: express$Request, res: express$Response) => {
    return SakerModel.findAll({
        order: [['tidsstempel', 'DESC']],
        limit: 15
    })
        .then(saker => res.send(saker));
})


// gets articles ordered by category
app.get('/sak/kategori/:kategori', (req: express$Request, res: express$Response) => {
    return SakerModel.findAll({
        where: {kategori: String(req.params.kategori)},
        limit: 35
    })
        .then(sak => sak ? res.send(sak) : res.sendStatus(404));
});


export let listen = new Promise<void>((resolve, reject) => {
    // Wait for Sequalize to connect to and initialize the database
    syncModels.then(() => {
        let call_listen = reloader => {
            app.listen(3001, (error: ?Error) => {
                if (error) reject(error.message);
                console.log('Express server started');
                // Start hot reload (refresh web page on client changes) when not in production environment
                if (reloader) {
                    reloader.reload(); // Reload application on server restart
                    fs.watch(public_path, () => reloader.reload());
                }
                resolve();
            });
        };
        // Setup hot reload (refresh web page on client changes) when not in production environment,
        // and then start the web server.
        if (process.env.NODE_ENV != 'production') reload(app).then(reloader => call_listen(reloader));
        else call_listen();
    });
});