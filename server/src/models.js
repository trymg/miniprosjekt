// @flow

import Sequelize from 'sequelize';
import type {Model} from 'sequelize';

// Connect to mysql-database
let sequelize = new Sequelize('trymg', 'trymg', 'HbuurOJK', {
    host: process.env.CI ? 'mysql.stud.iie.ntnu.no' : 'mysql.stud.iie.ntnu.no', // The host is 'mysql' when running in gitlab CI
    dialect: 'mysql',
    define: {
        freezeTableName: true,
        timestamps: false
    }
});

// Types for database table sak
export type Sak = {
    sak_id?: number,
    overskrift: string,
    innhold: string,
    // tidsstempel: string,
    bildelink: string,
    kategori: string, //Teknologi, Sport, Musikk
    alt_tekst: string,
    viktighet: number
};

// Create database table sak
export let SakerModel: Class<Model<Sak>> = sequelize.define('Saker', {
    sak_id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    overskrift: Sequelize.STRING,
    innhold: Sequelize.STRING,
    tidsstempel: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    bildelink: Sequelize.STRING,
    kategori: Sequelize.STRING,
    alt_tekst: Sequelize.STRING,
    viktighet: Sequelize.INTEGER
});

// Drop tables and create test data when not in production environment
let production = process.env.NODE_ENV === 'production';
// The sync promise can be used to wait for the database to be ready (for instance in your tests)
export let syncModels = sequelize.sync({force: production ? false : true}).then(() => {
    // Create test data when not in production environment
    if (!production)
        return SakerModel.create({
            overskrift: 'jiha1',
            innhold: 'innholdTest',
            bildelink: 'https://hookagency.com/wp-content/uploads/2017/07/hide-the-pain-harold-stock-photo.jpg',
            kategori: 'sport', //Teknologi, Sport, Musikk
            alt_tekst: 'altTest',
            viktighet: 1
        })
        .then(() =>
            SakerModel.create({
                overskrift: 'jiha2',
                innhold: 'innholdTest',
                bildelink: 'https://hookagency.com/wp-content/uploads/2017/07/hide-the-pain-harold-stock-photo.jpg',
                kategori: 'sport', //Teknologi, Sport, Musikk
                alt_tekst: 'altTest',
                viktighet: 1
            })
        );
});