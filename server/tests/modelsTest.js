// @flow

import {type Sak, SakerModel, syncModels} from '../src/models.js';

beforeAll(() => syncModels);

describe('Saker test', () => {
    it('correct data', done => {
        SakerModel.findAll().then(saker => {
            expect(
                saker
                    .map(sak => sak.toJSON())
                    .map((sak: Sak) => ({
                        sak_id: sak.sak_id,
                        overskrift: sak.overskrift,
                        innhold: sak.innhold,
                        // tidsstempel: sak.tidsstempel,
                        bildelink: sak.bildelink,
                        kategori: sak.kategori,
                        alt_tekst: sak.alt_tekst,
                        viktighet: sak.viktighet
                    }))
            ).toEqual([
                {
                    sak_id: 1,
                    overskrift: 'jiha1',
                    innhold: 'innholdTest',
                    bildelink: 'https://hookagency.com/wp-content/uploads/2017/07/hide-the-pain-harold-stock-photo.jpg',
                    kategori: 'sport', //Teknologi, Sport, Musikk
                    alt_tekst: 'altTest',
                    viktighet: 1
                },
                {
                    sak_id: 2,
                    overskrift: 'jiha2',
                    innhold: 'innholdTest',
                    bildelink: 'https://hookagency.com/wp-content/uploads/2017/07/hide-the-pain-harold-stock-photo.jpg',
                    kategori: 'sport', //Teknologi, Sport, Musikk
                    alt_tekst: 'altTest',
                    viktighet: 1
                }
            ]);
            done();
        });
    });
});


describe('Sak test', () => {
    it('correct data', done => {
        SakerModel.findOne().then(sak => {
            expect(
                sak
                    .map(sak => sak.toJSON())
                    .map((sak: Sak) => ({
                        sak_id: sak.sak_id,
                        overskrift: sak.overskrift,
                        innhold: sak.innhold,
                        bildelink: sak.bildelink,
                        kategori: sak.kategori,
                        alt_tekst: sak.alt_tekst,
                        viktighet: sak.viktighet
                    }))
            ).toEqual(
                {
                    sak_id: 1,
                    overskrift: 'jiha1',
                    innhold: 'innholdTest',
                    bildelink: 'https://hookagency.com/wp-content/uploads/2017/07/hide-the-pain-harold-stock-photo.jpg',
                    kategori: 'sport', //Teknologi, Sport, Musikk
                    alt_tekst: 'altTest',
                    viktighet: 1
                }
            );
            done();
        });
    });
});