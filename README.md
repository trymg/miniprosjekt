# React example with static pages

## Prerequisites
* Flow and Prettier installed globally:
```sh
npm install -g flow-bin prettier
```
* Editor with Flow and prettier support

## Fetch, install dependencies, and run
```sh
git clone https://gitlab.stud.idi.ntnu.no/trymg/miniprosjekt.git
cd miniprosjekt
cd server
npm install
npm run start-prod

cd ../client
npm install
npm run build-prod
```

## Open application
http://localhost:3000


npm install cors --save